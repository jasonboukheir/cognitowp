# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Simple WP server with PHPMyAdmin using Docker Compose. SSL must be set up manually.

### How do I get set up? ###

You need Docker with Docker Compose and git command line.

run: 

git clone https://jasonboukheir@bitbucket.org/jasonboukheir/cognitowp.git

cd cognitowp

docker-compose up -d 

Then wait. After waiting, you can access your WP server heading to localhost, and your PHPMyAdmin heading to localhost:8080. Your root pw and un are:

un: root

pw: secret

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact